package edu.luc.etl.cs313.android.shapes.android;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import java.util.List;
import edu.luc.etl.cs313.android.shapes.model.*;

/**
 * A Visitor for drawing a shape to an Android canvas.
 */
public class Draw implements Visitor<Void> {

	// TODO entirely your job (except onCircle)

	private final Canvas canvas;

	private final Paint paint;

	public Draw(final Canvas canvas, final Paint paint) {
		this.canvas = canvas; // FIXME - fixed
		this.paint = paint; // FIXME - fixed
		paint.setStyle(Style.STROKE);
	}

	@Override
	public Void onCircle(final Circle c) {
		canvas.drawCircle(0, 0, c.getRadius(), paint);
		return null;
	}

	@Override
	public Void onStrokeColor(final StrokeColor c) {
		this.paint.setColor(c.getColor());
		c.getShape().accept(this);
		return null;
	}

	@Override
	public Void onFill(final Fill f) {
		this.paint.setStyle(Style.FILL_AND_STROKE);
		f.getShape().accept(this);
		return null;
	}

	@Override
	public Void onGroup(final Group g) {
		for(Shape s: g.getShapes()) {
			s.accept(this);
		}
		return null;
	}

	@Override
	public Void onLocation(final Location l) {
		canvas.translate(l.getX(), l.getY());
		l.getShape().accept(this);
		canvas.translate(l.getX() * -1, l.getY()* -1 );
		paint.setStyle(Style.STROKE);
		paint.setColor(-16777216);
		return null;
	}

	@Override
	public Void onRectangle(final Rectangle r) {
		canvas.drawRect(0,0,r.getWidth(),r.getHeight(), paint);
		return null;
	}

	@Override
	public Void onOutline(Outline o) {
		this.paint.setStyle(Style.STROKE);
		o.getShape().accept(this);
		this.paint.setStyle(Style.FILL_AND_STROKE);
		paint.setColor(-16777216);
		return null;

	}

	@Override
	public Void onPolygon(final Polygon s) {
	final List<? extends Point> list = s.getPoints();
	int size = 4 * list.size();
	int track = 2;

		final float[] pts = new float[size];

		pts[0] = list.get(0).getX();
		pts[1] = list.get(0).getY();
		pts[14] = list.get(0).getX();
		pts[15] = list.get(0).getY();

		for(int j = 1; j < list.size(); j++) {
			for(int i = track; i < track + 4; i++) {
				if (i % 2 ==0) {
					pts[i] = list.get(j).getX();
				} else {
					pts[i] = list.get(j).getY();
				}
			}

			track = track+ 4;
		}

		canvas.drawLines(pts, paint);
		paint.setColor(-16777216);
		return null;
	}
}
