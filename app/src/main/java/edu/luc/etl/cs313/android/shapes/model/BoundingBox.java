package edu.luc.etl.cs313.android.shapes.model;

import java.util.List;

/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO entirely your job (except onCircle) - DONE

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		final List<? extends Shape> shapes = g.getShapes();
		Location firstBox = shapes.get(0).accept(this);
		Rectangle r = (Rectangle) firstBox.getShape();
		int upperLeftX = firstBox.getX();
		int upperLeftY = firstBox.getY();
		int lowerRightX = firstBox.getX() + r.getWidth();
		int lowerRightY = firstBox.getY() + r.getHeight();
		for (int i=1;i<shapes.size();i++){
			Location nextBox = shapes.get(i).accept(this);
			Rectangle newR = (Rectangle) nextBox.getShape();
			int newUpperLeftX = nextBox.getX();
			int newUpperLeftY = nextBox.getY();
			int newLowerRightX = nextBox.getX() + newR.getWidth();
			int newLowerRightY = nextBox.getY() + newR.getHeight();
			upperLeftX = Math.min(upperLeftX,newUpperLeftX);
			upperLeftY = Math.min(upperLeftY,newUpperLeftY);
			lowerRightX = Math.max(lowerRightX, newLowerRightX);
			lowerRightY = Math.max(lowerRightY,newLowerRightY);
		}
		return new Location(upperLeftX, upperLeftY, new Rectangle(lowerRightX-upperLeftX,lowerRightY-upperLeftY));
	}

	@Override
	public Location onLocation(final Location l) {
		final Location box = l.getShape().accept(this);
		return new Location(box.getX()+l.getX(),box.getY()+l.getY(), box.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int width = r.getWidth();
		final int height = r.getHeight();
		return new Location(0,0, new Rectangle(width,height));
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		return onGroup(s);
	}
}
